create table if not exists articles(
    tid char(36) primary key,
    title text not null ,
    hook varchar(50) not null,
    content text not null,
    article_state text not null,
    user_tid char(36)  references users(tid) not null
);

create table if not exists users(
    tid char(36) primary key,
    email text not null unique,
    pseudo text not null
);
create table if not exists categorie(
    name text not null unique,
    article_tid char(36)  references articles(tid) not null
);