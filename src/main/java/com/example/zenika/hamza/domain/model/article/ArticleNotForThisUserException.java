package com.example.zenika.hamza.domain.model.article;

public class ArticleNotForThisUserException extends Exception {
    public ArticleNotForThisUserException() {
        super("The article does not belong to this user");
    }
}
