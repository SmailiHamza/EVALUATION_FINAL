package com.example.zenika.hamza.domain.model.article;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="articles")
@Getter

@NoArgsConstructor
public class Article {
    @Id
    private String tid;
    @Setter
    private String title;
    @Setter
    private String hook;
    @Setter
    private String content;
    @Setter
    private ArticleState articleState;
    @Setter
    private String userTid;


    public Article(String tid, String title, String hook, String content) {
        this.tid = tid;
        this.title = title;
        this.hook = hook;
        this.content = content;
        this.articleState=ArticleState.DRAFT;
        this.userTid="310eff07-3a2a-4a78-9a90-23f3948eb50f";
    }
}
