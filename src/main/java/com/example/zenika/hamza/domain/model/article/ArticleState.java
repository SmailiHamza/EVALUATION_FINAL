package com.example.zenika.hamza.domain.model.article;

public enum ArticleState {
    DRAFT,PUBLISHED
}
