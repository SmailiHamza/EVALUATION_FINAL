package com.example.zenika.hamza.domain.model.user;

import org.springframework.util.StringUtils;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class User {
    @Id
    private String tid;
    private String email;
    private String pseudo;

    public User(){
        //for JDA
    }

    public User(String tid, String email, String username) {
        this.tid = tid;
        this.pseudo = username;
        this.email = email;
    }

    public String getTid() {
        return tid;
    }

    public String getUsername() {
        return pseudo;
    }

    public void setUsername(String username) {
        if(!StringUtils.hasText(username)) {
            throw new IllegalArgumentException("Username can't be null");
        }
        this.pseudo = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if(!StringUtils.hasText(email)) {
            throw new IllegalArgumentException("Email can't be null");
        }
        this.email = email;
    }
}
