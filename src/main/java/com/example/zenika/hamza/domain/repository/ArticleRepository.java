package com.example.zenika.hamza.domain.repository;

import com.example.zenika.hamza.domain.model.article.Article;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ArticleRepository extends CrudRepository<Article,String> {
    public List<Article> findByUserTid(String userTid);
}
