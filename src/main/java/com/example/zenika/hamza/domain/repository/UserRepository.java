package com.example.zenika.hamza.domain.repository;


import com.example.zenika.hamza.domain.model.user.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User,String> {
   public Optional<User> findByEmail(String email);
}
