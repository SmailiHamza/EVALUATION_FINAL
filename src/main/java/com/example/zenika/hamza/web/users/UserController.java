package com.example.zenika.hamza.web.users;


import com.example.zenika.hamza.application.UserService;
import com.example.zenika.hamza.domain.model.article.Article;
import com.example.zenika.hamza.domain.model.user.EmailAlreadyUsedException;
import com.example.zenika.hamza.domain.model.user.User;
import com.example.zenika.hamza.domain.model.user.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    ResponseEntity<User> createUser(@RequestBody CreateUserRequestDto body) {
        User createdUser = userService.createUser(body.email(), body.pseudo());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
    }

    @GetMapping("/{userTid}")
    ResponseEntity<User> findUser(@PathVariable String userTid) {
        return userService.findByTid(userTid).map(u -> ResponseEntity.ok(u)).orElseGet(() -> ResponseEntity.notFound().build());
    }
    @GetMapping("/{userTid}/articles")
    List<Article> findUserArticles(@PathVariable String userTid) {
        return userService.findArticles(userTid);
    }

    @GetMapping
    List<User> searchUsers(@RequestParam("email") String email) {
        return userService.findByEmail(email)
                .map(List::of)
                .orElseGet(Collections::emptyList);
    }

    @PatchMapping("/{userTid}")
    ResponseEntity<Void> changeUsername(@PathVariable String userTid, @RequestBody ChangeUsernameRequestDto body) {
        try {
            userService.changeUsername(userTid, body.newPseudo());
            return ResponseEntity.ok().build();
        } catch (UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{userTid}")
    @ResponseStatus(HttpStatus.OK)
    void deleteUser(@PathVariable String userTid) {
        userService.deleteUser(userTid);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleIllegalArgument(IllegalArgumentException e) {
        return "Missing information : " + e.getMessage();
    }
    
    @ExceptionHandler
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleIllegalArgument(EmailAlreadyUsedException e) {
        return "Email already exists";
    }
}
