package com.example.zenika.hamza.web.users;

public record CreateUserRequestDto(String email, String pseudo) {
}
