package com.example.zenika.hamza.web.articles;

public record CreateArticleRequestDto(String title, String hook,String content,String userTid) {
}
