package com.example.zenika.hamza.web.articles;

import com.example.zenika.hamza.application.ArticleManager;
import com.example.zenika.hamza.domain.model.article.Article;
import com.example.zenika.hamza.domain.model.article.ArticleNotForThisUserException;
import com.example.zenika.hamza.domain.model.user.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/article")
@CrossOrigin(origins = "*")
public class ArticleController {
    private final ArticleManager articleManager;

    public ArticleController(ArticleManager articleManager) {
        this.articleManager = articleManager;
    }

    @PostMapping
    ResponseEntity<Article> createArticle(@RequestBody CreateArticleRequestDto body) throws UserNotFoundException {
        Article createdArticle = this.articleManager.save(body.title(), body.hook(),body.content(),body.userTid());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdArticle);
    }
    @PutMapping("/{articleTid}")
    ResponseEntity<Article> updateArticle(@RequestBody CreateArticleRequestDto body,@PathVariable String articleTid) throws UserNotFoundException, ArticleNotForThisUserException {
        Article createdArticle = this.articleManager.update(articleTid,body.title(), body.hook(),body.content(),body.userTid());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdArticle);
    }
    @PostMapping("/{articleTid}")
    ResponseEntity<Article> updateArticle(@PathVariable String articleTid) {
        Article createdArticle = this.articleManager.publish(articleTid);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdArticle);
    }
    @GetMapping("/{articleTid}")
    ResponseEntity<Article> findArticle(@PathVariable String articleTid) {
        return this.articleManager.findByTid(articleTid).map(u -> ResponseEntity.ok(u)).orElseGet(() -> ResponseEntity.notFound().build());
    }
    @GetMapping
    Iterable<Article> findAllArticle() {
        return this.articleManager.findAll();
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleUserNotFound(UserNotFoundException e) {
        return "User not found";
    }
    @ExceptionHandler
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String handleForbidden(ArticleNotForThisUserException e) {
        return "Forbidden : " + e.getMessage();
    }
}
