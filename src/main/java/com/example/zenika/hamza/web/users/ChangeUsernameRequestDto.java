package com.example.zenika.hamza.web.users;

public record ChangeUsernameRequestDto(String newPseudo) {
}
