package com.example.zenika.hamza.application;


import com.example.zenika.hamza.domain.model.article.Article;
import com.example.zenika.hamza.domain.model.user.EmailAlreadyUsedException;
import com.example.zenika.hamza.domain.model.user.User;
import com.example.zenika.hamza.domain.model.user.UserNotFoundException;
import com.example.zenika.hamza.domain.repository.ArticleRepository;
import com.example.zenika.hamza.domain.repository.UserRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class UserService {

    private final UserRepository userRepository;
    private final ArticleRepository articleRepository;

    public UserService(UserRepository userRepository, ArticleRepository articleRepository) {
        this.userRepository = userRepository;
        this.articleRepository = articleRepository;
    }

    public User createUser(String email, String username) {
        User newUser = new User(UUID.randomUUID().toString(), email, username);
        try{
            userRepository.save(newUser);
        }catch (Exception e){
            throw new EmailAlreadyUsedException();
        }

        
        return newUser;
    }

    public Optional<User> findByTid(String userTid) {
        return userRepository.findById(userTid);
    }

    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void changeUsername(String userTid, String newUsername) throws UserNotFoundException {
        User user = userRepository.findById(userTid).orElseThrow(UserNotFoundException::new);
        user.setUsername(newUsername);
        userRepository.save(user);
    }

    public void deleteUser(String userTid) {
        userRepository.deleteById(userTid);
    }

    public List<Article> findArticles(String userTid) {
        return articleRepository.findByUserTid(userTid);
    }
}
