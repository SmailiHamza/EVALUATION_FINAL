package com.example.zenika.hamza.application;

import com.example.zenika.hamza.domain.model.article.Article;
import com.example.zenika.hamza.domain.model.article.ArticleNotForThisUserException;
import com.example.zenika.hamza.domain.model.article.ArticleState;
import com.example.zenika.hamza.domain.model.user.UserNotFoundException;
import com.example.zenika.hamza.domain.repository.ArticleRepository;
import com.example.zenika.hamza.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class ArticleManager {
    private final ArticleRepository articleRepository;
    private final UserRepository userRepository;
    @Autowired
    public ArticleManager(ArticleRepository articleRepository, UserRepository userRepository) {
        this.articleRepository = articleRepository;
        this.userRepository = userRepository;
    }
    public Article save(String title, String hook,String content,String userTid) throws UserNotFoundException {
        Article newArticle = new Article(UUID.randomUUID().toString(), title, hook, content);
        if (userTid!=null){
            userRepository.findById(userTid).orElseThrow(UserNotFoundException::new);
            newArticle.setUserTid(userTid);
        }else{
            throw new UserNotFoundException();
        }
        this.articleRepository.save(newArticle);
        return newArticle;
    }
    public Optional<Article> findByTid(String tid) {
        return this.articleRepository.findById(tid);
    }
    public Iterable<Article> findAll() {
        return this.articleRepository.findAll();
    }
    public Article update(String tid,String title, String hook,String content,String userTid) throws UserNotFoundException, ArticleNotForThisUserException {
        Article article = this.articleRepository.findById(tid).orElseThrow();
        if (userTid!=null){
            userRepository.findById(userTid).orElseThrow(UserNotFoundException::new);
           if( articleRepository.findByUserTid(userTid).stream().
                   filter(a -> a.getTid().equals(tid)).
                   collect(Collectors.toList()).
                   isEmpty())
           {
               throw new ArticleNotForThisUserException();
           }
        }else{
            throw new UserNotFoundException();
        }
        if(article.getArticleState().equals(ArticleState.DRAFT)){
            article.setTitle(title);
            article.setHook(hook);
        }
        article.setContent(content);
        this.articleRepository.save(article);
        return article;
    }
    public Article publish(String tid) {
        Article article = this.articleRepository.findById(tid).orElseThrow();
        article.setArticleState(ArticleState.PUBLISHED);
        this.articleRepository.save(article);
        return article;
    }
    public List<Article> findByUserTid(String userTid){
        return articleRepository.findByUserTid(userTid);
    }
}
